'use strict';

const HTTPStatus = require('http-status');
const AdvancedError = require('@da-fat-company/advanced-error');

/**
 * Simple Lambda Wrapper
 * It provide an abstraction over the lambda callback and Promises runtimes
 */
class LambdaWrapper {
  /**
   * Constructor
   * @param {Function} finalCallback AWS Lambda callback function
   */
  constructor(finalCallback) {
    if (typeof finalCallback !== 'function') {
      throw new AdvancedError({
        title: 'LambdaWrapper Internal Error',
        message: 'LambdaWrapper first arg, finalCallback, must be a function',
        privacy: AdvancedError.IS_PRIVATE
      });
    }

    this.finalCallback = finalCallback;
    this.statusCode = LambdaWrapper.DEFAULT_STATUS_CODE;
    this.body = LambdaWrapper.DEFAULT_BODY;
    this.headers = LambdaWrapper.DEFAULT_HEADERS;
    this.promise = null;
  }

  /**
   * Set headers
   *
   * @param {Object} headers Header to be sent to AWS Lamdba
   * @return {LambdaWrapper} Self instance
   */
  setHeaders(headers) {
    this.headers = headers;

    return this;
  }

  /**
   * Set default body sent in case of success
   *
   * @param {Object} body Object to send as JSON
   * @return {LambdaWrapper} Self instance
   */
  setDefaultSuccessBody(body) {
    this.body = body;

    return this;
  }

  /**
   * Run a function wrapped into a Promise
   *
   * @param {Function} func Function to call over a Promise
   *
   * @returns {LambdaWrapper} This instance
   */
  run(func) {
    if (this.promise === null) {
      this.promise = LambdaWrapper.promisify(func);
    } else {
      this.promise = this.promise.then(data => LambdaWrapper.promisify(func, data));
    }

    return this;
  }

  /**
   * Run a list of function wrapped into Promises
   *
   * @param {Array} funcList Functions to call over Promises
   *
   * @returns {LambdaWrapper} This instance
   */
  runAll(funcList) {
    return this.run(
      (data, resolve, reject) => LambdaWrapper.bindPromise(
        () => LambdaWrapper.promisifyAll(funcList, data), resolve, reject)
    );
  }

  /**
   * Build and return an AWS Lambda formated response
   *
   * @returns {Object} AWS Lambda formated response
   */
  getResponse() {
    return {
      headers: this.headers,
      statusCode: this.statusCode,
      body: JSON.stringify(this.body), // Always stringify Objects inside the 'body'
    };
  }

  /**
   * Terminate the process by sending responses
   * Add the error handler to promise chaining process
   *
   * @param {Function?} customCatchFunction Function that handle Promise Catch
   * @param {Function?} customFinallyFunction Function that handle the final Promise then
   * @return {LambdaWrapper} This
   */
  end(customCatchFunction, customFinallyFunction) {
    let catchFunction = customCatchFunction;

    if (!catchFunction || typeof catchFunction !== 'function') {
      catchFunction = err => {
        const error = AdvancedError.createFromError(err);

        this.statusCode = error.status;
        this.body.success = false;
        if (error.privacy === AdvancedError.IS_PUBLIC) {
          this.body.error = error.title;
          this.body.error_message = error.message;
        } else {
          this.body.error = 'Internal server error';
        }
      };
    }
    let finallyFunction = customFinallyFunction;

    if (!finallyFunction || typeof finallyFunction !== 'function') {
      finallyFunction = data => {
        // Handle Promise returned data into the final body
        if (data) {
          // Feel free to add custom data modifier here or as the last .run()
          this.body = data;
        }
        this.finalCallback(null, this.getResponse());
      };
    }

    this.promise.catch(catchFunction).then(finallyFunction);

    return this;
  }

  /**
   * Execute a function inside a promise
   *
   * @param {Function} func Function to call over a Promise
   * @param {Object | String?} data Datas to pass through the function
   *
   * @return {Promise} Return the Promise itself
   */
  static promisify(func, data) {
    return new Promise((resolve, reject) => {
      try {
        const funcResponse = func(data, resolve, reject);

        if (funcResponse instanceof Promise) {
          return funcResponse.then(data => resolve(data))
            .catch(err => reject(err));
        } else {
          return resolve(funcResponse);
        }
      } catch (error) {
        reject(AdvancedError.createFromError(error));

        return false;
      }
    });
  }

  /**
   * Execute a list of functions promisifying them all
   *
   * @param {Array} funcList Functions list
   * @param {Object | String?} data Datas to pass through the function
   *
   * @return {Promise} Return the Promise itself
   */
  static promisifyAll(funcList, data) {
    const promiseList = [];

    funcList.forEach(func => {
      promiseList.push(LambdaWrapper.promisify(func, data));
    });

    return Promise.all(promiseList);
  }

  /**
   * Wrap a promise resolving and executing passed functions
   *
   * @param {Function} func Function to call
   * @param {Function} resolve Resolve function to call inside .then()
   * @param {Function} reject Reject function to call inside .catch()
   * @return {Undefined} Void
   */
  static bindPromise(func, resolve, reject) {
    const funcResponse = func();

    if (funcResponse instanceof Promise) {
      funcResponse.then(data => resolve(data))
        .catch(err => reject(AdvancedError.createFromError(err)));
    }

    return funcResponse;
  }
}

LambdaWrapper.DEFAULT_HEADERS = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*'
};
LambdaWrapper.DEFAULT_BODY = {
  success: true
};
LambdaWrapper.DEFAULT_STATUS_CODE = HTTPStatus.OK;


module.exports = LambdaWrapper;
