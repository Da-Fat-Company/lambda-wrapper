# Lambda Wrapper

[![NPM Badge](https://img.shields.io/npm/v/@da-fat-company/lambda-wrapper.svg)](https://www.npmjs.com/package/@da-fat-company/lambda-wrapper)
[![build status](https://gitlab.com/Da-Fat-Company/lambda-wrapper/badges/master/build.svg)](https://gitlab.com/Da-Fat-Company/lambda-wrapper/commits/master)
[![coverage report](https://gitlab.com/Da-Fat-Company/lambda-wrapper/badges/master/coverage.svg)](https://da-fat-company.gitlab.io/lambda-wrapper/coverage)
[![JsDoc report](https://img.shields.io/badge/jsdoc-link-green.svg)](https://da-fat-company.gitlab.io/lambda-wrapper/jsdoc)
[![Plato report](https://img.shields.io/badge/plato-link-green.svg)](https://da-fat-company.gitlab.io/lambda-wrapper/plato)

NodeJS App Wrapper to help developper fastly build AWS Lambda based application.

This package provide a helper class base on promise chaining and manage the lambda response.

The wrapper send data in JSON in response to a lambda call. (Default is { "success": true } ).

## Install

```bash
    $ npm i --save @da-fat-company/lambda-wrapper
```

## Usage

### Basics
```javascript
'use strict';

const LambdaWrapper = require('@da-fat-company/lambda-wrapper');

module.exports.run = (event, context, callback) => {

  const app = new LambdaWrapper(callback);

  const bar = [];

  // Each run is a chained promise
  app
  .run(() => { console.log('Do something here'); })
  .run(() => { 
    console.log('Then return a value here');
    return 'foo'; 
  })  
  .run((data) => { 
    console.log('Then retreive the previously returned value as first arg', data); // foo
    return Promise.resolve(data);
  })
  .run((data, resolve, rejeect) => { 
    console.log('Then eventually do something more complexe or async, that require a resolver/rejecter');
    setTimeout(() => resolve(data), 1000);
  })
  .runAll([
    () => console.log('Do something async'),
    () => bar.push(1),
    () => bar.push(2),
    () => bar.push(3)
  ])
  .run(() => console.log(bar)) // [1, 2, 3]
  .end(); // End function handle error and finally  
};
```

By default `end` function catch errors and convert them into AdvancedError, then set the lambda body to:

If AdvancedError privacy is private (default case):
```json
{
  "success": false,
  "error": "Internal Server Error"
}
```

If AdvancedError privacy is public:
```json
{
  "success": false,
  "error": "Error Title",
  "error_message": "Error Message"
}
```

### Provide custom catch / finally function

The `end` function can take two arguments, the first one is a custom catch function, the second one a finally function.

```javascript
'use strict';

const LambdaWrapper = require('@da-fat-company/lambda-wrapper');

module.exports.run = (event, context, callback) => {

  const app = new LambdaWrapper(callback);

  // Function called on catch
  const myErrorHandler = (error) => {
    console.log('Do something with the error: ', error);
    
    app.body.success = false;
  };
  
  // Function called at the end (even if the catch function has been invoked)
  const myFinalFunction = (data) => {
    console.log('This is the end!', data);
  };

  app
  .run(() => { console.log('Do something here'); })  
  .end(myErrorHandler, myFinalFunction);
};
```

### Change default success body
By default the wrapper send as JSON output the folowing object:

```json 
{
    "success": true    
}
```

You can change it using `setDefaultSuccessBody()`:

```javascript
'use strict';

const LambdaWrapper = require('@da-fat-company/lambda-wrapper');

module.exports.run = (event, context, callback) => {

  const app = new LambdaWrapper(callback);

  app
  .setDefaultSuccessBody({
    foo: 'bar'
  })
  .run(() => { console.log('Do something here'); })  
  .end(); // Response will be { "foo": "bar" }
};
```

But if the last `run()` call resolve or return an object, this data is passed directly to the body. 
Note that a `JSON.stringify()` is applyied to the body before sending it to lambda callback.


```javascript
'use strict';

const LambdaWrapper = require('@da-fat-company/lambda-wrapper');

module.exports.run = (event, context, callback) => {

  const app = new LambdaWrapper(callback);

  app
  .run(() => { 
    return {
      foo: "bar"
    }
  })
  .end(); // Response will be { "foo": "bar" }
};
```

### Change headers
By default, the wrapper set the following header to lambda response:
* `Content-Type`: `application/json`
* `Access-Control-Allow-Origin`: `*`

But you can change them using `setHeaders()`:


```javascript
'use strict';

const LambdaWrapper = require('@da-fat-company/lambda-wrapper');

module.exports.run = (event, context, callback) => {

  const app = new LambdaWrapper(callback);

  app.setHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': 'example.io'
  })
  .run(() => { console.log('Do something here'); })  
  .end();
};
```