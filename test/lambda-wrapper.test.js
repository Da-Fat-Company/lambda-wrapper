'use strict';

const AdvancedError = require('@da-fat-company/advanced-error');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

const expect = chai.expect;
const LambdaWrapper = require('../index');

describe('LambdaWrapper Tests', function() {
  it('Should fail if no callback function provided', () => {
    expect(() => new LambdaWrapper()).to.throw(AdvancedError);
  });

  it('Should run and end by calling the initial callback', () => {
    let finished = false;
    const app = new LambdaWrapper(() => {
      finished = true;
    });

    app.run(() => true).end().run(() => {
      expect(app.promise).to.be.fulfilled.then(() => {
        expect(finished).to.be.true;
      });
    });
  });

  it('Should run and end by calling the initial callback using a promise as run() return value', () => {
    let finished = false;
    const app = new LambdaWrapper(() => {
      finished = true;
    });

    app.run(() => Promise.resolve(true)).end().run(() => {
      expect(app.promise).to.be.fulfilled.then(() => {
        expect(finished).to.be.true;
      });
    });
  });

  it('Should run and end with a custom finally function', done => {
    let finished = false;
    const app = new LambdaWrapper(() => {
      finished = true;
    });

    app.run(() => true).end(null, () => {
      app.finalCallback();
      expect(app.promise).to.be.fulfilled.then(() => {
        expect(finished).to.equal(true);
        done();
      });
    });
  });

  it('Should fail with default catch function and a private error (by default)', () => {
    const app = new LambdaWrapper(() => {});

    app.run(() => { throw new Error('error'); }).end(null, () => {
      expect(app.body).to.be.an('object');
      expect(app.body).to.have.deep.property('success', false);
      expect(app.body).to.have.deep.property('error', 'Internal server error');
    });
  });

  it('Should fail with default catch function and a private error (by default) using a promise as run() return value', () => {
    const app = new LambdaWrapper(() => {});

    app.run(() => Promise.reject(new Error('error'))).end(null, () => {
      expect(app.body).to.be.an('object');
      expect(app.body).to.have.deep.property('success', false);
      expect(app.body).to.have.deep.property('error', 'Internal server error');
    });
  });

  it('Should fail with default catch function and a public error', done => {
    const app = new LambdaWrapper(() => {});

    app.run(() => {
      throw new AdvancedError({
        title: 'custom error',
        message: '...',
        privacy: AdvancedError.IS_PUBLIC
      });
    }).end(null, () => {
      try {
        expect(app.body).to.be.an('object');
        expect(app.body).to.have.deep.property('success', false);
        expect(app.body).to.have.deep.property('error', 'custom error');

        done();
      } catch (err) {
        done(err);
      }
    });
  });

  it('Should fail run and end with a custom catch function', () => {
    const app = new LambdaWrapper(() => {});

    app.run(() => { throw new Error('error'); }).end(err => {
      expect(err).to.be.an.instanceof(AdvancedError);
    });
  });

  it('Should run and provide (data, resolve, reject)', done => {
    const app = new LambdaWrapper(() => {
      done();
    });

    app.run(() => 'test' )
       .run((data, resolve, reject) => {
         expect(data).to.be.a.string('test');
         expect(resolve).to.be.a('function');
         expect(reject).to.be.a('function');

         resolve(true);
       })
       .end();
  });

  it('Should set custom headers and default success body', done => {
    let finished = false;
    const app = new LambdaWrapper(() => {
      finished = true;
    });

    app
      .setHeaders({
        'Content-Type': 'text/html'
      })
      .setDefaultSuccessBody({
        finished: true
      })
      .run(() => true).end(null, () => {
        app.finalCallback();
        expect(app.promise).to.be.fulfilled.then(() => {
          expect(app.body).to.be.an('object');
          expect(app.body).to.have.deep.property('finished', true);

          expect(app.headers).to.be.an('object');
          expect(app.headers).to.have.deep.property('Content-Type', 'text/html');

          expect(finished).to.equal(true);
          done();
        });
      });
  });

  it('Should run several function asynchronously using runAll()', () => {
    let finished = false;
    const app = new LambdaWrapper(() => {
      finished = true;
    });
    const list = [];

    app.runAll([
      () => {list.push(1);},
      () => {list.push(2);},
      () => {list.push(3);}
    ]).end().run(() => {
      expect(app.promise).to.be.fulfilled.then(() => {
        expect(finished).to.be.true;
        expect(list).to.have.members([1, 2, 3]);
      });
    });
  });

  it('Should fail to run several function asynchronously using runAll()', (done) => {
    const app = new LambdaWrapper(() => {});
    const list = [];
    let successTest = false;

    app.runAll([
      () => {list.push(1);},
      () => {return Promise.reject(new Error('test'));},
      () => {list.push(3);}
    ]).end(err => {
      try {
        expect(err).to.be.an.instanceof(AdvancedError);
        successTest = true;
      } catch (err) {
        done(err);
      }
    }, () => {
      try {
        expect(list).to.have.members([1, 3]);

        if (successTest) {
          done();
        } else {
          done(new Error('Failure not handled in runAll()'));
        }
      } catch (err) {
        done(err);
      }
    });
  });

  it('Should use bindPromise() without a promise', () => {
    expect(LambdaWrapper.bindPromise(() => true)).to.be.true;
  });
});

